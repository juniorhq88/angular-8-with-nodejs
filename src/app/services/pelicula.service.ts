import { Injectable } from '@angular/core';
import { Pelicula } from '../models/pelicula';

@Injectable()
export class PeliculaService{

	public peliculas: Array<Pelicula>;


	constructor(){
		this.peliculas = [
		      new Pelicula(2019,'Spiderman 4','https://www.jakpost.travel/wimages/large/0-3454_3d-hd-wallpapers-hd-amazing-spider-man-2.jpg'),
		      new Pelicula(2019,'Los vengadores Endgame','https://e.rpp-noticias.io/normal/2019/03/28/383238_771346.jpg'),
		      new Pelicula(2019,'Batman vs Superman','https://www.moviezone.cz/obr/YXJ0aWNsZU1haW4vMTkyMzg5'),
		      new Pelicula(2016,'Spiderman 4','https://www.jakpost.travel/wimages/large/0-3454_3d-hd-wallpapers-hd-amazing-spider-man-2.jpg'),
		      new Pelicula(2019,'Los vengadores Endgame','https://e.rpp-noticias.io/normal/2019/03/28/383238_771346.jpg'),
		      new Pelicula(2015,'Batman vs Superman','https://www.moviezone.cz/obr/YXJ0aWNsZU1haW4vMTkyMzg5')

		    ];
	}

	holaMundo(){
		return 'Hola Mundo desde el servicio peliculas de Angular';
	}

	getPeliculas(){
		return this.peliculas;
	}
}