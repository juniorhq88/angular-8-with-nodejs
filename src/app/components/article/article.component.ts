import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import Swal from 'sweetalert2';

import { ArticleService } from '../../services/article.service';
import { Article } from '../../models/article';
import { Global } from '../../services/global';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css'],
  providers: [ArticleService]
})
export class ArticleComponent implements OnInit {

	public article: Article;
	public url: string;

  constructor(
  	private _articleService: ArticleService,
  	private _route: ActivatedRoute,
  	private _router: Router
  	) { 
  	this.url = Global.url;
  }

  ngOnInit() {
  	this._route.params.subscribe(params => {
  		let id = params['id'];
  		
  		this._articleService.getArticle(id).subscribe(
  			response => {
  				if(response.article){
  					this.article = response.article;
  					 console.log(response);
  				}else{
  					this._router.navigate(['/home']);
  				}
  			},
  			error => {
  				console.log(error);
  				this._router.navigate(['/home']);
  			}
  			);
  	});
  	
  }

  delete(id){

    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover article!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if(result.value){

        this._articleService.delete(id).subscribe(
          response => {
            Swal.fire("Deleted !","Your article has been delete correctly !!!","success");
            this._router.navigate(['/blog']);
          },
          error => {
            console.log(error);
            this._router.navigate(['/blog']);
          }
          );

        
      }else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire("Cancelled !!!","Your article was not deleted","error");
       }
  });
    
  }

}
